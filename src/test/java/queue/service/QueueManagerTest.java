package queue.service;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import queue.domain.WorkOrderImpl;

/**
 * Created by Bryan on 03/09/2018.
 */
public class QueueManagerTest {

    private DateTime dtNow = new DateTime(2018, 6, 6, 21, 0, 0);

    private QueueManager queueManager;
    private int currentNormalID = 0;
    private int currentPriorityID = 0;
    private int currentVIPID = 0;
    private int currentManagementOverrideID = 0;

    @Before
    public void setup(){
        this.queueManager = new QueueManager(){
            @Override
            protected DateTime getUtcNow() {
                return dtNow;
            }
        };
    }

    @Test
    public void testSimple() {
        WorkOrderImpl woNormal =    new WorkOrderImpl(getNextAvailableID(RequestorStatus.NORMAL), dtNow);
        WorkOrderImpl woPriority =  new WorkOrderImpl(getNextAvailableID(RequestorStatus.PRIORITY), dtNow);
        WorkOrderImpl woVIP =       new WorkOrderImpl(getNextAvailableID(RequestorStatus.VIP), dtNow);
        WorkOrderImpl woMO =        new WorkOrderImpl(getNextAvailableID(RequestorStatus.MANAGEMENT_OVERRIDE), dtNow);
        queueManager.add(woNormal);
        queueManager.add(woMO);
        queueManager.add(woPriority);
        queueManager.add(woVIP);

        Assert.assertTrue(queueManager.getWorkOrder(woNormal.getRequestorID()).equals(woNormal));
        Assert.assertTrue(queueManager.getWorkOrder(woPriority.getRequestorID()).equals(woPriority));
        Assert.assertTrue(queueManager.getWorkOrder(woVIP.getRequestorID()).equals(woVIP));
        Assert.assertTrue(queueManager.getWorkOrder(woMO.getRequestorID()).equals(woMO));

        Assert.assertTrue(queueManager.getHighestRankingWorkOrder().equals(woMO));

        Assert.assertTrue(queueManager.getRank(woNormal, dtNow, woMO.getTimestamp()) == 0);
        Assert.assertTrue(queueManager.getRank(woPriority, dtNow, woMO.getTimestamp()) == QueueManager.PRIORITY_BASE_RANK);
        Assert.assertTrue(queueManager.getRank(woVIP, dtNow, woMO.getTimestamp()) == QueueManager.VIP_BASE_RANK);
        Assert.assertTrue(queueManager.getRank(woMO, dtNow, woMO.getTimestamp()) == QueueManager.MAX_RANK);
    }

    @Test
    public void testRankNormalWorkOrders(){
        DateTime dt1 = new DateTime(2018, 6, 6, 9, 15, 0);
        DateTime dt2 = new DateTime(2018, 6, 6, 9, 40, 0);
        DateTime dt3 = new DateTime(2018, 6, 6, 10, 20, 0);
        DateTime dt4 = new DateTime(2018, 6, 6, 13, 30, 0);
        DateTime dtNow = new DateTime(2018, 6, 6, 15, 0, 0);
        WorkOrderImpl wo1 = new WorkOrderImpl(getNextAvailableID(RequestorStatus.NORMAL), dt1);
        WorkOrderImpl wo2 = new WorkOrderImpl(getNextAvailableID(RequestorStatus.NORMAL), dt2);
        WorkOrderImpl wo3 = new WorkOrderImpl(getNextAvailableID(RequestorStatus.NORMAL), dt3);
        WorkOrderImpl wo4 = new WorkOrderImpl(getNextAvailableID(RequestorStatus.NORMAL), dt4);
        queueManager.add(wo4);
        queueManager.add(wo2);
        queueManager.add(wo1);
        queueManager.add(wo3);
        Assert.assertTrue(queueManager.getHighestRankingWorkOrder().equals(wo1));
        int rank1 = queueManager.getRank(wo1, dtNow, null);
        int rank2 = queueManager.getRank(wo2, dtNow, null);
        int rank3 = queueManager.getRank(wo3, dtNow, null);
        int rank4 = queueManager.getRank(wo4, dtNow, null);
        Assert.assertEquals(20700, rank1);
        Assert.assertEquals(19200, rank2);
        Assert.assertEquals(16800, rank3);
        Assert.assertEquals(5400, rank4);
    }

    @Test
    public void testRankNormalPriorityAndVIPWorkOrders(){

        WorkOrderImpl woN = new WorkOrderImpl(getNextAvailableID(RequestorStatus.NORMAL), dtNow.minusDays(1));
        WorkOrderImpl woP = new WorkOrderImpl(getNextAvailableID(RequestorStatus.PRIORITY), dtNow.minusHours(1));
        WorkOrderImpl woV = new WorkOrderImpl(getNextAvailableID(RequestorStatus.VIP), dtNow.minusMinutes(30));
        queueManager.add(woP);
        queueManager.add(woV);
        queueManager.add(woN);

        /**
         * We have a NORMAL task from 1 day ago, a PRIORITY task from 1 hour ago and a VIP task from 30 minutes ago.
         * Assert the normal task has highest priority and the vip task has lowest
         */
        Assert.assertEquals(86400, queueManager.getRank(woN, dtNow, null));
        Assert.assertEquals(39600, queueManager.getRank(woP, dtNow, null));
        Assert.assertEquals(36000, queueManager.getRank(woV, dtNow, null));
        Assert.assertTrue(queueManager.getHighestRankingWorkOrder().equals(woN));

        /**
         * Fast forward 3 hours. Now the Normal task should have lowest priority and the VIP task highest priority
         */
        dtNow = dtNow.plusHours(3);
        Assert.assertEquals(97200, queueManager.getRank(woN, dtNow, null));
        Assert.assertEquals(187200, queueManager.getRank(woP, dtNow, null));
        Assert.assertEquals(327600, queueManager.getRank(woV, dtNow, null));
        Assert.assertTrue(queueManager.getHighestRankingWorkOrder().equals(woV));
    }

    @Test
    public void testRankManagementOverrideWorkOrders(){

        WorkOrderImpl woM1 = new WorkOrderImpl(getNextAvailableID(RequestorStatus.MANAGEMENT_OVERRIDE), dtNow.minusMinutes(90));
        WorkOrderImpl woM2 = new WorkOrderImpl(getNextAvailableID(RequestorStatus.MANAGEMENT_OVERRIDE), dtNow.minusMinutes(60));
        WorkOrderImpl woM3 = new WorkOrderImpl(getNextAvailableID(RequestorStatus.MANAGEMENT_OVERRIDE), dtNow.minusMinutes(30));
        queueManager.add(woM1);
        queueManager.add(woM2);
        queueManager.add(woM3);

        Assert.assertTrue(queueManager.getHighestRankingWorkOrder().equals(woM1));
        Assert.assertEquals(QueueManager.MAX_RANK, queueManager.getRank(woM1, dtNow, woM1.getTimestamp()));
        Assert.assertEquals(QueueManager.MAX_RANK - (60 * 30), queueManager.getRank(woM2, dtNow, woM1.getTimestamp()));
        Assert.assertEquals(QueueManager.MAX_RANK - (60 * 60), queueManager.getRank(woM3, dtNow, woM1.getTimestamp()));
    }

    //region helper methods

    private int getNextAvailableID(RequestorStatus requestorStatus){
        switch (requestorStatus){
            default:
            case NORMAL:
                while (true){
                    currentNormalID++;
                    if(RequestorStatus.getRequestorStatus(currentNormalID).equals(requestorStatus)){
                        break;
                    }
                }
                return currentNormalID;
            case PRIORITY:
                while (true){
                    currentPriorityID++;
                    if(RequestorStatus.getRequestorStatus(currentPriorityID).equals(requestorStatus)){
                        break;
                    }
                }
                return currentPriorityID;
            case VIP:
                while (true){
                    currentVIPID++;
                    if(RequestorStatus.getRequestorStatus(currentVIPID).equals(requestorStatus)){
                        break;
                    }
                }
                return currentVIPID;
            case MANAGEMENT_OVERRIDE:
                while (true){
                    currentManagementOverrideID++;
                    if(RequestorStatus.getRequestorStatus(currentManagementOverrideID).equals(requestorStatus)){
                        break;
                    }
                }
                return currentManagementOverrideID;
        }
    }

    //endregion
}
