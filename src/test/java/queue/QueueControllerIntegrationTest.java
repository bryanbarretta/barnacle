package queue;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import queue.domain.WorkOrderImpl;
import queue.domain.WorkOrderRequest;

/**
 * Created by Bryan on 29/08/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QueueControllerIntegrationTest {

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    DateTime dt1 = new DateTime(2018, 6, 6, 6, 30, 0, DateTimeZone.UTC);
    DateTime dt2 = new DateTime(2018, 6, 6, 7, 30, 0, DateTimeZone.UTC);
    DateTime dt3 = new DateTime(2018, 6, 6, 8, 30, 0, DateTimeZone.UTC);

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/queue/");
        clearQueue();
        postWorkOrder(1, dt1);
        postWorkOrder(2, dt2);
        postWorkOrder(3, dt3);
    }

    /**
     * (1) An endpoint for adding a ID to queue (enqueue).
     */
    @Test
    public void testPost() throws Exception {
        ResponseEntity<String> response = postWorkOrder(274, dt3.plusDays(1));
        Assert.assertNull(response.getBody());
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertEquals(4, getWorkOrders().getBody().size());
    }

    @Test
    public void testPostConflict() throws Exception {
        ResponseEntity<String> response = postWorkOrder(1, dt3.plusDays(1));
        Assert.assertNull(response.getBody());
        Assert.assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        Assert.assertEquals(3, getWorkOrders().getBody().size());
    }

    @Test
    public void testPostBadRequest() throws Exception {
        ResponseEntity<String> response = postWorkOrder(0, dt3.plusDays(1));
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        JSONObject json = new JSONObject(response.getBody());
        JSONObject error = (JSONObject)((JSONArray)json.get("errors")).get(0);
        Assert.assertEquals(error.getString("defaultMessage"), "must be greater than or equal to 1");
        Assert.assertEquals(3, getWorkOrders().getBody().size());
    }

    /**
     * (2) An endpoint for getting the top ID from the queue and removing it (dequeue)
     */
    @Test
    public void testDequeueTop() throws Exception {
        ResponseEntity<WorkOrderImpl> response = deleteTopWorkOrder();
        Assert.assertEquals(1, response.getBody().getRequestorID());
        Assert.assertEquals(dt1, response.getBody().getTimestamp());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDequeueTopEmpty() throws Exception {
        ResponseEntity<WorkOrderImpl> response = deleteTopWorkOrder();
        Assert.assertEquals(1, response.getBody().getRequestorID());
        Assert.assertEquals(dt1, response.getBody().getTimestamp());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        response = deleteTopWorkOrder();
        Assert.assertEquals(2, response.getBody().getRequestorID());
        Assert.assertEquals(dt2, response.getBody().getTimestamp());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        response = deleteTopWorkOrder();
        Assert.assertEquals(3, response.getBody().getRequestorID());
        Assert.assertEquals(dt3, response.getBody().getTimestamp());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        response = deleteTopWorkOrder();
        Assert.assertNull(response.getBody());
        Assert.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    /**
     * (3) GET the list of IDs in the queue.
     */
    @Test
    public void testGet() throws Exception {
        ResponseEntity<List> response = getWorkOrders();
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(3, response.getBody().size());
        JSONArray arr = new JSONArray(response.getBody());
        JSONObject j1 = (JSONObject) arr.get(0);
        JSONObject j2 = (JSONObject) arr.get(1);
        JSONObject j3 = (JSONObject) arr.get(2);
        WorkOrderImpl w1 = new WorkOrderImpl(j1.getInt("requestorID"), new DateTime(j1.getString("timestamp"), DateTimeZone.UTC));
        WorkOrderImpl w2 = new WorkOrderImpl(j2.getInt("requestorID"), new DateTime(j2.getString("timestamp"), DateTimeZone.UTC));
        WorkOrderImpl w3 = new WorkOrderImpl(j3.getInt("requestorID"), new DateTime(j3.getString("timestamp"), DateTimeZone.UTC));
        Assert.assertEquals(1, w1.getRequestorID());
        Assert.assertEquals(dt1, w1.getTimestamp());
        Assert.assertEquals(2, w2.getRequestorID());
        Assert.assertEquals(dt2, w2.getTimestamp());
        Assert.assertEquals(3, w3.getRequestorID());
        Assert.assertEquals(dt3, w3.getTimestamp());
    }

    /**
     * (4) An endpoint for removing a specific ID from the queue.
     */
    @Test
    public void testDequeueID() throws Exception {
        ResponseEntity<WorkOrderImpl> response = deleteWorkOrder(1);
        Assert.assertEquals(1, response.getBody().getRequestorID());
        Assert.assertEquals(dt1, response.getBody().getTimestamp());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDequeueNonExistingID() throws Exception {
        ResponseEntity<WorkOrderImpl> response = deleteWorkOrder(76);
        Assert.assertNull(response.getBody());
        Assert.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    /**
     * (5) An endpoint to get the position of a speciﬁc ID in the queue.
     */
    @Test
    public void testGetPositionID() throws Exception {
        ResponseEntity<Integer> response = getWorkOrderPosition(1);
        Assert.assertEquals((Integer) 0, response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetPositionNonExistingID() throws Exception {
        ResponseEntity<Integer> response = getWorkOrderPosition(84);
        Assert.assertNull(response.getBody());
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * (6) An endpoint to get the average wait time.
     */
    @Test
    public void testGetAverageWaitTime() throws Exception {
        DateTime dtUtcNow = dt3.plusHours(2);

        ResponseEntity<Integer> response = getAverageWaitTime(dtUtcNow);
        Assert.assertEquals((Integer) 10800, response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        //Verify the average time has changed an hour later
        dtUtcNow = dtUtcNow.plusHours(1);
        response = getAverageWaitTime(dtUtcNow);
        Assert.assertEquals((Integer) 14400, response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        //Verify the average time is different after dequeue
        deleteTopWorkOrder();
        response = getAverageWaitTime(dtUtcNow);
        Assert.assertEquals((Integer) 12600, response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetAverageWaitTimeEmptyQueue() throws Exception {
        clearQueue();
        ResponseEntity<Integer> response = getAverageWaitTime(dt3);
        Assert.assertEquals((Integer) 0, response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetAverageWaitTimeBadRequest() throws Exception {
        clearQueue();
        ResponseEntity<Integer> response = getAverageWaitTime("2018-Jan-01-3:30pm");
        Assert.assertNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    //region helper methods

    private void clearQueue(){
        ResponseEntity<List> orders = getWorkOrders();
        for(Object order : orders.getBody()){
            deleteWorkOrder((int)((LinkedHashMap) order).get("requestorID"));
        }
    }

    private ResponseEntity<String> postWorkOrder(int id, DateTime timestamp){
        return template.postForEntity(base.toString(), new WorkOrderRequest(id, timestamp), String.class);
    }

    private ResponseEntity<WorkOrderImpl> deleteTopWorkOrder(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        return template.exchange(base.toString() + "top", HttpMethod.DELETE, entity, WorkOrderImpl.class);
    }

    private ResponseEntity<List> getWorkOrders(){
        return template.getForEntity(base.toString(), List.class);
    }

    private ResponseEntity<WorkOrderImpl> deleteWorkOrder(int id){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        return template.exchange(base.toString() + id, HttpMethod.DELETE, entity, WorkOrderImpl.class);
    }

    private ResponseEntity<Integer> getWorkOrderPosition(int id){
        return template.getForEntity(base.toString() + id, Integer.class);
    }

    private ResponseEntity<Integer> getAverageWaitTime(DateTime timestamp){
        return getAverageWaitTime(timestamp.toString());
    }

    private ResponseEntity<Integer> getAverageWaitTime(String timestamp){
        return template.getForEntity(base.toString() + "wait/" + timestamp, Integer.class);
    }

    //endregion

}
