package queue.service;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import queue.domain.WorkOrder;
import queue.domain.WorkOrderRequest;
import queue.store.QueueStore;
import queue.domain.WorkOrderImpl;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

/**
 * Business logic layer of the application.
 * Service that the QueueController uses to run operations and return requested objects/data.
 *  - The QueueStore is used to read/write work orders from/to the database
 *  - The QueueManager is used to manage the work orders in memory: sorting them based on the ranking algorithm
 * The QueueManager should load data from the store on initialization. After that, read's come solely from the QueueManager.
 * When a write/delete operation is performed, update both the queue store and the manager.
 * Created by Bryan on 02/09/2018.
 */
public class QueueService {

    @Autowired
    private QueueStore queueStore;

    private QueueManager queueManager;

    private boolean isLoaded = false;

    /**
     * Initialise the QueueManager and load database work orders using the QueueStore into it
     */
    public QueueService(){
        queueManager = new QueueManager();
    }

    @PostConstruct
    public void setup(){
        if(!isLoaded) {
            Iterable<WorkOrderImpl> dbWorkOrders = queueStore.findAll();
            for (WorkOrderImpl dbWorkOrder : dbWorkOrders) {
                queueManager.add(dbWorkOrder);
            }
            isLoaded = true;
        }
    }

    public List<WorkOrder> getWorkOrders() {
        return queueManager.getWorkOrders();
    }

    /**
     * Adds a work order to the queue
     * @param request
     * @return false if the requestor already has a work order in the queue. Otherwise return true.
     */
    public boolean enqueueWorkOrder(WorkOrderRequest request) {
        if(queueManager.contains(request.getRequestorID())){
            return false;
        }else {
            WorkOrderImpl wo = new WorkOrderImpl();
            wo.setRequestorID(request.getRequestorID());
            wo.setTimestamp(request.getTimestamp());
            add(wo);
            return true;
        }
    }

    /**
     * Remove the highest ranking Work Order from the queue and return it
     * @return null if there are no work orders in the queue
     */
    public WorkOrder dequeueTopWorkOrder() {
        WorkOrder top = queueManager.getHighestRankingWorkOrder();
        delete(top);
        return top;
    }

    /**
     * Remove the Work Order with the corresponding requestorid
     * @return null if there are no work orders in the queue matching the id
     */
    public WorkOrder dequeueWorkOrder(int requestorid) {
        WorkOrder workOrder = queueManager.getWorkOrder(requestorid);
        delete(workOrder);
        return workOrder;
    }

    /**
     * Gets the 0-based index position of the requested id work order in the queue
     * @return null if the queue does not contain the requested id
     */
    public Integer getPosition(int requestorid) {
        List<WorkOrder> orders = queueManager.getWorkOrders();
        OptionalInt index = IntStream.range(0, orders.size()).filter(i -> requestorid == orders.get(i).getRequestorID()).findFirst();
        return index.isPresent() ? index.getAsInt() : null;
    }

    /**
     * Given a timestamp, return the average number of seconds each order has been waiting based on that timestamp
     * @param timestamp
     * @return integer representing the average number of seconds each order has been waiting
     */
    public Integer getAverageWaitTime(DateTime timestamp) {
        List<WorkOrder> orders = queueManager.getWorkOrders();
        int seconds = 0;
        for(WorkOrder wo : orders){
            if(timestamp.isBefore(wo.getTimestamp())){
                continue;
            }
            seconds += Seconds.secondsBetween(wo.getTimestamp(), timestamp).getSeconds();
        }
        return seconds == 0 ? 0 : seconds / orders.size();
    }

    //region add | delete

    private void add(WorkOrder workOrder){
        if(workOrder != null) {
            queueStore.save((WorkOrderImpl) workOrder);
            queueManager.add(workOrder);
        }
    }

    private void delete(WorkOrder workOrder){
        if(workOrder != null) {
            queueStore.delete((WorkOrderImpl) workOrder);
            queueManager.remove(workOrder);
        }
    }

    //endregion
}
