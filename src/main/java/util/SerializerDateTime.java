package util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;

/**
 * Outputs a shorthand String representation of a joda datetime object in the response as the raw joda datetime output is long and overkill for this service
 * Created by Bryan on 03/09/2018.
 */
public class SerializerDateTime extends JsonSerializer<DateTime> {
    @Override
    public void serialize(DateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (dateTime == null){
            jsonGenerator.writeNull();
        }
        else {
            jsonGenerator.writeString(ISODateTimeFormat.dateTime().print(dateTime.withZone(DateTimeZone.UTC)));
        }
    }
}
