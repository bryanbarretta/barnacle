  ____          _____  _   _          _____ _      ______ 
 |  _ \   /\   |  __ \| \ | |   /\   / ____| |    |  ____|
 | |_) | /  \  | |__) |  \| |  /  \ | |    | |    | |__   
 |  _ < / /\ \ |  _  /| . ` | / /\ \| |    | |    |  __|  
 | |_) / ____ \| | \ \| |\  |/ ____ \ |____| |____| |____ 
 |____/_/    \_\_|  \_\_| \_/_/    \_\_____|______|______|
                                                          
                                                          
 Guide on how to setup this project locally
 1. Software Requirements
 2. Prerequisites
 3. Deployment
 4. Running Tests
 5. Endpoints
 5. Extra Information
 
 Diagram overview of the class setup: https://www.dropbox.com/s/pg4kbajql3u16eg/barnacle.jpg?dl=0 

----------------------------------------------
 1. Software Requirements
----------------------------------------------

 To run this project locally you will need:

 - Java 1.8
 - Gradle
 - SQL Server (or another datasource supported in Spring Boot). I used Microsoft SQL Server Management Studio (Express edition) : https://www.microsoft.com/en-ie/sql-server/sql-server-editions-express)

 Recommended
 
 - Intellij (or similar Java IDE). I used the free Community edition: https://www.jetbrains.com/idea/download/#section=windows)
 - Postman (or similar API testing tool)
 
----------------------------------------------
 2. Prerequisites 
----------------------------------------------

 The only prerequisite is that the datasource value defined in the Applications.properties file exists. By default, this is:
 
	spring.datasource.url=jdbc:sqlserver://127.0.0.1;databaseName=barnacle
	spring.datasource.username=springuser
	spring.datasource.password=1
 
 This means that (unless you change these values), you will need a sqlserver database called 'barnacle' on your localhost server (127.0.0.1) with a login ['springuser'/'1'].
 Note you can use any datasource supported in Spring Boot as long as you update the Application.properties file, but I have used Microsoft SQL Server for this setup.
 Here I will explain how to set this up from scratch, change the server/databaseName/username/password if desired:

 - Log into Microsoft SQL Server Management Studio with your default credentials
 - Ensure SQL Server is setup to support SQL Server authentication (by default it only supports windows authentication): https://www.dundas.com/support/learning/documentation/install-configure/how-to-enable-sql-server-authentication
 - Expand local server and right click Security->New->Login...
   - Use SQL Server Authentication radio button. Enter a user name/password (e.g. 'springuser'/'1') 
   - In the Server Roles tab along the left, check 'sysadmin'/'db_owner' box to grant all permissions
   - Click OK to create. Open a new connection to your local server as this user to verify it is working.
 - Setup SQL Server to allow jdbc connection, as per the 'SQL Server Configuration' section of this guide: https://springframework.guru/configuring-spring-boot-for-microsoft-sql-server/
 - Right click Databases->New Database... and give it a name (e.g. 'barnacle')
  
----------------------------------------------
 3. Deployment 
----------------------------------------------

 From the command line (Using git bash on a windows machine):
  - cd PROJECT_ROOT_DIRECTORY
  - ./gradlew stop 														[optional command to stop any running instances]
  - ./gradlew build && java -jar build/libs/gs-spring-boot-0.1.0.jar	[this will build the jar file and run it]

 From Intellij IDE:
  - Open the gradle window under: View > Tool Windows > Gradle
  - Barnacle > Tasks > build > build
  - After build is finished, go to the Project explorer and expand Barnacle > build > libs
  - Right click gs-spring-boot-0.1.0.jar... You can choose to run or debug it here
  
----------------------------------------------
 4. Running 
----------------------------------------------

 By default the base URL will be "http://localhost:8080/" if you are running on your localhost; The system output will display the port it started on:
	Tomcat started on port(s): 8080 (http) with context path ''

 [Swagger]
 Swagger documentation for the endpoints available at http://localhost:8080/swagger-ui.html

 [Postman]
 If you have postman, you can hit all the endpoints. See section '5. Endpoints' below for request format.

 [Intellij or other Java IDE]
 Under Project Explorer->src, right click 'test' folder and choose 'Run All Tests'
   
----------------------------------------------
 5. Endpoints
----------------------------------------------
   
Below are the 6 endpoints as defined in the document. Each endpoint has the number and description, HTTPMethod, URL, raw json body.
For every endpoint, you need to add 2 headers:
   Content-Type : application/json
   Accept : application/json
For timestamps I am using Joda DateTime: http://joda-time.sourceforge.net/apidocs/org/joda/time/DateTime.html 

...................................................   
 
 (1) Adds an ID to queue (enqueue). 
 POST 
 http://localhost:8080/queue
 {
	"requestorID": 3,
	"timestamp": "2018-09-03T11:37:15.153+0000"
 }
 
...................................................
 
 (2) Get the top ID from the queue and remove it (dequeue)
 DELETE 
 http://localhost:8080/queue/top
 
...................................................

 (3) Get the list of IDs in the queue, sorted by highest ranking to lowest
 GET 
 http://localhost:8080/queue
 
...................................................

 (4) Remove a specific ID from the queue
 DELETE 
 http://localhost:8080/queue/{REQUESTOR_ID}
 
...................................................

 (5) Get the position of a specific ID in the queue
 GET http://localhost:8080/queue/{REQUESTOR_ID}
 
...................................................

 (6) Get the average wait time (mean) of all the work orders in the queue in seconds
 GET http://localhost:8080/queue/wait/{TIMESTAMP}
 
----------------------------------------------
 6. Extra Information
----------------------------------------------

 [Database initialization]
 In the Applications.properties file, note this value by default: 
    spring.jpa.hibernate.ddl-auto=create-drop
 'create-drop' means the database is dropped and created every time the application starts up. Once created, you can change this 
 value to 'none' to persist the database once after shutdown/next start up. 
 For more information, see https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html 
 
