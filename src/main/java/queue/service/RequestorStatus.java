package queue.service;

import static queue.domain.WorkOrder.MAX_ID;
import static queue.domain.WorkOrder.MIN_ID;

/**
 * Category of the Requestor based on their ID.
 * Created by Bryan on 03/09/2018.
 */
public enum RequestorStatus {

    NORMAL,
    PRIORITY,
    VIP,
    MANAGEMENT_OVERRIDE;

    /**
     * (1) IDs that are evenly divisible by 3 are priority IDs.
     * (2) IDs that are evenly divisible by 5 are VIP IDs.
     * (3) IDs that are evenly divisible by both 3 and 5 are management override IDs.
     * (4) IDs that are not evenly divisible by 3 or 5 are normal IDs.
     */
    public static RequestorStatus getRequestorStatus(long id){
        if(id < MIN_ID || id > MAX_ID){
            return null;
        }
        boolean isPriority  = id % 3 == 0 && ((id / 3) % 2 == 0);
        boolean isVIP       = id % 5 == 0 && ((id / 5) % 2 == 0);
        boolean isManagementOverride = isPriority & isVIP;
        if(isManagementOverride){
            return MANAGEMENT_OVERRIDE;
        }
        if(isVIP){
            return VIP;
        }
        if(isPriority){
            return PRIORITY;
        }
        return NORMAL;
    }

}
