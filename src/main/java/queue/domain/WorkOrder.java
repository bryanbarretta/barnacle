package queue.domain;

import org.joda.time.DateTime;

/**
 * Interface that work order requests must implement for use on this web service
 * Created by Bryan on 29/08/2018.
 */
public interface WorkOrder {

    public static final long MIN_ID = 1;
    public static final long MAX_ID = 9223372036854775807L;

    int getRequestorID();

    DateTime getTimestamp();

}
