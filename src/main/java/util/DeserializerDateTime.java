package util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;

/**
 * Reads a DateTime object in from a String representation of a joda datetime object in the users input request
 * Created by Bryan on 03/09/2018.
 */
public class DeserializerDateTime extends JsonDeserializer<DateTime> {
    @Override
    public DateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        try {
            return ISODateTimeFormat.dateTime().withZoneUTC().parseDateTime(jsonParser.getText());
        } catch (Exception ex) {
            throw ex;
        }
    }
}
