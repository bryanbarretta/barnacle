package queue.domain;

import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Implementation of the WorkOrder interface used for accessing the database
 * Created by Bryan on 02/09/2018.
 */
@Entity
@Table(name = "work_orders")
public class WorkOrderImpl implements WorkOrder {

    @Id
    @Column(name = "requestor_id")
    private int requestorID;

    @Column(name = "timestamp")
    private DateTime timestamp;

    public WorkOrderImpl(){

    }

    public WorkOrderImpl(int requestorID, DateTime timestamp){
        this.requestorID = requestorID;
        this.timestamp = timestamp;
    }

    public int getRequestorID() {
        return requestorID;
    }

    public void setRequestorID(int requestorID) {
        this.requestorID = requestorID;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }
}
