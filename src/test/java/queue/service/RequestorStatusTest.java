package queue.service;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Bryan on 03/09/2018.
 */
public class RequestorStatusTest {

    @Test
    public void testInvalidIds(){
        Assert.assertNull(RequestorStatus.getRequestorStatus(-1));
        Assert.assertNull(RequestorStatus.getRequestorStatus(0));
    }

    /**
     * IDs that are evenly divisible by 3 are priority IDs.
     */
    @Test
    public void testPriority(){
        Assert.assertTrue(RequestorStatus.getRequestorStatus(3) != RequestorStatus.PRIORITY);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(6) == RequestorStatus.PRIORITY);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(9) != RequestorStatus.PRIORITY);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(12) == RequestorStatus.PRIORITY);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(15) != RequestorStatus.PRIORITY);

        //This is evenly divisible by 3, but also by 5 so it is ManagementOverride
        Assert.assertTrue(RequestorStatus.getRequestorStatus(30) != RequestorStatus.PRIORITY);
    }

    /**
     * IDs that are evenly divisible by 5 are VIP IDs.
     */
    @Test
    public void testVIP(){
        Assert.assertTrue(RequestorStatus.getRequestorStatus(5) != RequestorStatus.VIP);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(10) == RequestorStatus.VIP);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(15) != RequestorStatus.VIP);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(20) == RequestorStatus.VIP);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(25) != RequestorStatus.VIP);

        //This is evenly divisible by 5, but also by 3 so it is ManagementOverride
        Assert.assertTrue(RequestorStatus.getRequestorStatus(30) != RequestorStatus.VIP);
    }

    /**
     * IDs that are evenly divisible by both 3 and 5 are management override IDs.
     */
    @Test
    public void testManagementOverride(){
        //This is divisible by 5 and 3 but not an even number of times so it is not ManagementOverride
        Assert.assertTrue(RequestorStatus.getRequestorStatus(15) != RequestorStatus.MANAGEMENT_OVERRIDE);
        //This is evenly divisible by 5 and 3 so it is ManagementOverride
        Assert.assertTrue(RequestorStatus.getRequestorStatus(30) == RequestorStatus.MANAGEMENT_OVERRIDE);
        //This is divisible by 5 and 3 but not an even number of times so it is not ManagementOverride
        Assert.assertTrue(RequestorStatus.getRequestorStatus(45) != RequestorStatus.MANAGEMENT_OVERRIDE);
        //This is evenly divisible by 5 and 3 so it is ManagementOverride
        Assert.assertTrue(RequestorStatus.getRequestorStatus(60) == RequestorStatus.MANAGEMENT_OVERRIDE);
    }

    /**
     * IDs that are not evenly divisible by 3 or 5 are normal IDs.
     */
    @Test
    public void testNormal(){
        Assert.assertTrue(RequestorStatus.getRequestorStatus(1) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(2) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(3) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(4) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(5) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(6) != RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(7) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(8) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(9) == RequestorStatus.NORMAL);
        Assert.assertTrue(RequestorStatus.getRequestorStatus(10) != RequestorStatus.NORMAL);
    }
}
