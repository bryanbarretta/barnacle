package queue;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import queue.domain.WorkOrder;
import queue.domain.WorkOrderRequest;
import queue.service.QueueService;

import javax.validation.Valid;
import java.util.List;

/**
 * The queue controller handles service endpoints for managing the queue.
 * This class defines the endpoint url, request type & input/output type for each endpoint using spring annotation @RequestMapping.
 * No business logic is carried out here; requests are made to the QueueService and the response is passed back.
 * Created by Bryan on 29/08/2018.
 */

@RestController
@RequestMapping("/")
@Api(value="QueueControllerAPI",produces = MediaType.APPLICATION_JSON_VALUE)
public class QueueController {

    public final static String PARAM_REQUESTOR_ID = "requestorid";
    public final static String PARAM_TIMESTAMP = "timestamp";

    private QueueService queueService;

    Logger logger = LoggerFactory.getLogger(QueueController.class);

    public QueueController(QueueService queueService){
        this.queueService = queueService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @GetMapping("/")
    @ApiOperation("Testing")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Ok", response = String.class)})
    public String index() {
        return "Hello world";
    }

    /**
     * (1) An endpoint for adding a ID to queue (enqueue).
     * @param request contains the requestorID and the timestamp
     * @throws Exception
     */
    @RequestMapping(
            value = "queue",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ApiOperation("An endpoint for adding a ID to queue (enqueue)")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Successfully added a work order to the queue"),
                           @ApiResponse(code = 409, message = "Conflict: This requestor already has a work order in the queue")})
    public @ResponseBody
    ResponseEntity<String> enqueue(@RequestBody @Valid WorkOrderRequest request) throws Exception {
        logger.info("POST request to queue work order from requestor ID: {}", request.getRequestorID());
        if(queueService.enqueueWorkOrder(request)) {
            logger.info("Successful POST request to queue work order from requestor ID: {}", request.getRequestorID());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }else {
            logger.warn("Conflict encountered from POST request to queue work order from requestor ID: {}", request.getRequestorID());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    /**
     * (2) An endpoint for getting the top ID from the queue and removing it (dequeue)
     * @return the highest ranked ID and the time it was entered into the queue.
     */
    @RequestMapping(
            value = "queue/top",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ApiOperation("An endpoint for getting the top ID from the queue and removing it (dequeue)")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Queue is empty, no work order to dequeue"),
                           @ApiResponse(code = 200, message = "Successfully dequeued work order", response = WorkOrder.class)})
    public @ResponseBody
    ResponseEntity<WorkOrder> dequeueTopWorkOrder() throws Exception {
        logger.info("DELETE request to remove top work order from queue");
        WorkOrder result = queueService.dequeueTopWorkOrder();
        if(result != null) {
            logger.info("Successful DELETE request to remove top work order from queue [ID = {}]", result.getRequestorID());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }else {
            logger.warn("DELETE request to remove top work order from queue found no work orders in the queue!");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * (3) GET the list of IDs in the queue.
     * @return a list of IDs sorted from highest ranked to lowest.
     */
    @RequestMapping(
            value = "queue",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ApiOperation("GET the list of IDs in the queue")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "List of Work Orders sorted from highest ranked to lowest", response = WorkOrder.class)})
    public @ResponseBody
    ResponseEntity<List<WorkOrder>> getQueue(){
        logger.info("GET request to retrieve all work orders in queue");
        List<WorkOrder> workOrders = queueService.getWorkOrders();
        logger.info("GET request to retrieve all work orders in queue found {} work orders", workOrders.size());
        return new ResponseEntity<>(workOrders, HttpStatus.OK);
    }

    /**
     * (4) An endpoint for removing a specific ID from the queue.
     */
    @RequestMapping(
            value = "queue/{" + PARAM_REQUESTOR_ID + "}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ApiOperation("An endpoint for removing a speciﬁc ID from the queue.")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Queue is empty, no work order to dequeue"),
            @ApiResponse(code = 200, message = "Successfully dequeued work order", response = WorkOrder.class)})
    public @ResponseBody
    ResponseEntity<WorkOrder> dequeueWorkOrder(@PathVariable(value = PARAM_REQUESTOR_ID) int requestorid) throws Exception {
        logger.info("DELETE request to remove work order from queue [ID = {}]", requestorid);
        WorkOrder result = queueService.dequeueWorkOrder(requestorid);
        if(result != null) {
            logger.info("Successful DELETE request to remove work order from queue [ID = {}]", result.getRequestorID());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }else {
            logger.warn("DELETE request to remove work order [ID = {}] from queue found no work with matching ID in the queue!", requestorid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * (5) An endpoint to get the position of a speciﬁc ID in the queue.
     * @return the position of the ID in the queue indexed from 0.
     */
    @RequestMapping(
            value = "queue/{" + PARAM_REQUESTOR_ID + "}",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ApiOperation("An endpoint to get the position of a speciﬁc ID in the queue")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Position of work order in queue", response = Integer.class),
            @ApiResponse(code = 404, message = "No instance of specified Work Order in queue")})
    public @ResponseBody
    ResponseEntity<Integer> getPosition(@PathVariable(value = PARAM_REQUESTOR_ID) int requestorid){
        logger.info("GET request to find position of requestor id {} in queue", requestorid);
        Integer position = queueService.getPosition(requestorid);
        if(position != null) {
            return new ResponseEntity<>(position, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * (6) An endpoint to get the average wait time.
     * @param timestamp is a String that must be parsable by joda datetime e.g. "2018-09-03T21:27:37.467", "2018-09-03" etc.
     * @return the average (mean) number of seconds that each ID has been waiting in the queue.
     */
    @RequestMapping(
            value = "queue/wait/{" + PARAM_TIMESTAMP + "}",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ApiOperation("An endpoint to get the average wait time")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "The average (mean) number of seconds that each ID has been waiting in the queue", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad DateTime format parameter. Valid format examples: \"2018-09-03T21:27:37.467\", \"2018-09-03\" etc.")})
    public @ResponseBody
    ResponseEntity<Integer> getAverageWaitTime(@PathVariable(value = PARAM_TIMESTAMP) String timestamp){
        logger.info("GET request to find the average mean number of seconds each ID has been waiting");
        DateTime dt;
        try {
            dt = DateTime.parse(timestamp);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Integer seconds = queueService.getAverageWaitTime(dt);
        return new ResponseEntity<>(seconds, HttpStatus.OK);
    }

}
