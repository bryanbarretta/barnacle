package queue.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static queue.domain.WorkOrder.MAX_ID;
import static queue.domain.WorkOrder.MIN_ID;

/**
 * This object represents the JSON body of the Users request when they POST a work order.
 * Validation is also carried out here via javax validation annotations
 * Created by Bryan on 03/09/2018.
 */
public class WorkOrderRequest {

    @Min(MIN_ID)
    @Max(MAX_ID)
    @NotNull
    private Integer requestorID;

    @NotNull
    private DateTime timestamp;

    @JsonCreator
    public WorkOrderRequest(@JsonProperty(value = "requestorID", required = true) Integer requestorID,
                            @JsonProperty(value = "timestamp", required = true) DateTime timestamp) {
        this.requestorID = requestorID;
        this.timestamp = timestamp;
    }

    @JsonGetter("requestorID")
    public Integer getRequestorID() {
        return requestorID;
    }

    @JsonGetter("timestamp")
    public DateTime getTimestamp() {
        return timestamp;
    }
}
