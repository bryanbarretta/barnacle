package queue;

import org.joda.time.DateTime;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import queue.service.QueueService;
import util.DeserializerDateTime;
import util.SerializerDateTime;

/**
 * Created by Bryan on 29/08/2018.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public QueueService getQueueService(){
        return new QueueService();
    }

    /**
     * Manages shorthand string representations of joda datetime objects to make them more human-readable for the user
     */
    @Bean
    public  Jackson2ObjectMapperBuilder mapperBuilder(){
        return new Jackson2ObjectMapperBuilder()
                .deserializerByType(DateTime.class, new DeserializerDateTime())
                .serializerByType(DateTime.class, new SerializerDateTime());
    }
}
