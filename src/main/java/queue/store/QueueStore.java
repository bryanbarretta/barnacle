package queue.store;

import org.springframework.data.repository.CrudRepository;
import queue.domain.WorkOrderImpl;

/**
 * Persistence layer of the application.
 * Used to store and retrieve data directly from the data source db
 * Created by Bryan on 02/09/2018.
 */
public interface QueueStore extends CrudRepository<WorkOrderImpl, Integer> {

}
