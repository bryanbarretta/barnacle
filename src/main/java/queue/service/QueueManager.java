package queue.service;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import queue.domain.WorkOrder;

import java.util.*;

/**
 * Manage the ranking of work order requests in memory.
 * For Management Override status work orders:
 *  - The most critical (i.e. oldest) M.O. work order is given max rank (max integer) value: 2,147,483,647
 *  - Other M.O. work orders are given a ranking based off the max rank. So an M.O. order that is 20 seconds newer than the most critical
 *    M.O. order would get a ranking of 2,147,483,627
 * Created by Bryan on 03/09/2018.
 */
public class QueueManager {

    public static final int PRIORITY_BASE_RANK = 3;
    public static final int VIP_BASE_RANK = 4;
    public static final int MAX_RANK = Integer.MAX_VALUE;

    private List<WorkOrder> workOrders;

    public QueueManager(){
        this.workOrders = new ArrayList<>();
    }

    public List<WorkOrder> getWorkOrders(){
        sort();
        return workOrders;
    }

    public WorkOrder getWorkOrder(int requestorid){
        Optional<WorkOrder> wo = getWorkOrders().stream().filter(workOrder -> workOrder.getRequestorID() == requestorid).findAny();
        return wo.isPresent() ? wo.get() : null;
    }

    public WorkOrder getHighestRankingWorkOrder(){
        List<WorkOrder> orders = getWorkOrders();
        return orders.isEmpty() ? null : orders.get(0);
    }

    public void add(WorkOrder workOrder){
        this.workOrders.add(workOrder);
    }

    public boolean remove(WorkOrder workOrder){
        return this.workOrders.remove(workOrder);
    }

    public boolean contains(Integer requestorID) {
        return getWorkOrder(requestorID) != null;
    }

    /**
     * Sort the in memory array based on the defined algorithm at the top of this class, from highest ranking to lowest.
     * A final base timestamp is used before beginning the algorithm to compensate for time lost running the algorithm, so that
     * each work order is fairly ranked.
     * We need to sort the list each time the user requests to get data from it, because the rankings may change even without no
     * additional work orders. e.g. A new VIP work order will become more critical than an old NORMAL work order over time.
     */
    void sort(){
        final DateTime baseTime = getUtcNow();
        final DateTime oldestManagementOverrideTimestamp = getOldestManagementOverrideWorkOrderTimestamp();
        Collections.sort(workOrders, new Comparator<WorkOrder>() {
            @Override
            public int compare(WorkOrder o1, WorkOrder o2) {
                return getRank(o2, baseTime, oldestManagementOverrideTimestamp) - getRank(o1, baseTime, oldestManagementOverrideTimestamp);
            }
        });
    }

    /**
     * @return null if the queue does not have any management override work orders.
     *         Otherwise returns the timestamp of the oldest management override work order in the queue.
     */
    DateTime getOldestManagementOverrideWorkOrderTimestamp(){
        if(workOrders.isEmpty()){
            return null;
        }
        Optional<DateTime> oldestManagementOverrideTimestamp = workOrders.stream()
                .filter(workOrder -> RequestorStatus.getRequestorStatus(workOrder.getRequestorID()).equals(RequestorStatus.MANAGEMENT_OVERRIDE))
                .map(WorkOrder::getTimestamp).min(DateTime::compareTo);
        return oldestManagementOverrideTimestamp.isPresent() ? oldestManagementOverrideTimestamp.get() : null;
    }

    /**
     * (1) Normal IDs are given a rank equal to the number of seconds they’ve been in the queue.
     * (2) Priority IDs are given a rank equal to the result of applying the following formula to the number
     *     of seconds they’ve been in the queue: max(3,nlogn)
     * (3) VIP IDs are given a rank equal to the result of applying the following formula to the number of
     *     seconds they’ve been in the queue: max(4,2nlogn)
     * (4) Management Override IDs are always ranked ahead of all other IDs and are ranked among themselves
     *     according to the number of seconds they’ve been in the queue.
     * @param workOrder
     * @param baseTime this should be a utc now timestamp initialized before the comparison operation began
     * @param oldestManagementOverrideTimestamp This is the timestamp of the oldest management override work order in the queue (or
     *                                            null if there is none). Used to rank management override work orders.
     * @return an integer ranking of the workOrder argument
     */
    int getRank(WorkOrder workOrder, DateTime baseTime, DateTime oldestManagementOverrideTimestamp){
        RequestorStatus requestorStatus = RequestorStatus.getRequestorStatus(workOrder.getRequestorID());
        int secondsInQueue = Seconds.secondsBetween(workOrder.getTimestamp(), baseTime).getSeconds();
        switch (requestorStatus){
            default:
            case NORMAL:
                return secondsInQueue;
            case PRIORITY:
                return Math.max(PRIORITY_BASE_RANK, secondsInQueue * log_n(secondsInQueue));
            case VIP:
                return Math.max(VIP_BASE_RANK, 2 * secondsInQueue * log_n(secondsInQueue));
            case MANAGEMENT_OVERRIDE:
                if(oldestManagementOverrideTimestamp == null || workOrder.getTimestamp().equals(oldestManagementOverrideTimestamp)){
                    return MAX_RANK;
                }
                return MAX_RANK - Seconds.secondsBetween(oldestManagementOverrideTimestamp, workOrder.getTimestamp()).getSeconds();
        }
    }

    /**
     * Calculates the result of log n for a given value of n.
     * For reason for using "epsilon" when rounding, see: https://stackoverflow.com/a/3305400
     * @param n
     * @return log n
     */
    private int log_n(int n){
        return (int)(Math.log(n)/Math.log(2)+1e-10);
    }

    /**
     * Protected so we can override utc now in tests
     * @return
     */
    protected DateTime getUtcNow(){
        return DateTime.now();
    }
}
